# GitOps Dev Deploy Utility

This templates are to be used with the new GitLab Agent integration to make the integration as easy as possible.
Also it contains a way to deploy the devbranch automatically like a normal production environment without having to enable review deployments wich trigger on all branches.

## Integration with DEV branch
```yaml
include:
  - project: hostur2/gitops-dev-deploy-utility
    ref: main
    file: templates/dev.gitlab-ci.yml

variables:
  KUBE_NAMESPACE: "projectname"
  DEV_BRANCH: "dev"
```

## Default Integration 
```yaml
include:
  - project: hostur2/gitops-dev-deploy-utility
    ref: main
    file: templates/default.gitlab-ci.yml

variables:
  KUBE_NAMESPACE: "projectname"
```
